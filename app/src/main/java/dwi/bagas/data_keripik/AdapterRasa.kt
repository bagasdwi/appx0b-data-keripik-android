package dwi.bagas.data_keripik

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_rasa.*

class AdapterRasa(val dataRasa : List<HashMap<String,String>>, val rasaActivity: RasaActivity) :
    RecyclerView.Adapter<AdapterRasa.HolderDataRasa>(){

    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterRasa.HolderDataRasa {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_rasa,p0,false)
        return  HolderDataRasa(v)
    }

    override fun getItemCount(): Int {
        return dataRasa.size
    }

    override fun onBindViewHolder(holder: AdapterRasa.HolderDataRasa, position: Int) {
        val data = dataRasa.get(position)
        holder.txNamaRasa.setText(data.get("nama_rasa"))

        holder.cLayout.setOnClickListener({
            rasaActivity.idRasa = data.get("id_rasa").toString()
            rasaActivity.edNamaRasa.setText(data.get("nama_rasa"))
        })
    }

    class HolderDataRasa(v: View) : RecyclerView.ViewHolder(v) {
        val txNamaRasa = v.findViewById<TextView>(R.id.txNamaRasa)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}