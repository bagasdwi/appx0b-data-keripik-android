package dwi.bagas.data_keripik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnKrp ->{
                var mhs = Intent(this, KeripikActivity::class.java)
                startActivity(mhs)
                true
            }

            R.id.btnRasa ->{
                var prd = Intent(this, RasaActivity::class.java)
                startActivity(prd)
                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnKrp.setOnClickListener(this)
        btnRasa.setOnClickListener(this)
    }


}
