package dwi.bagas.data_keripik

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.actvity_keripik.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class KeripikActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var krpAdapter : AdapterDataKrp
    lateinit var rasaAdapter : ArrayAdapter<String>
    var daftarKrp = mutableListOf<HashMap<String,String>>()
    var daftarRasa = mutableListOf<String>()
    val mainUrl = "http://192.168.43.94/data_keripik/"
    val url = mainUrl+"show_data.php"
    var url2 = mainUrl+"get_nama_rasa.php"
    var url3 = mainUrl+"query_upd_del_ins.php"
    var imStr = ""
    var pilihRasa = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_keripik)
        krpAdapter = AdapterDataKrp(daftarKrp,this)
        mediaHelper = MediaHelper(this)
        listKrp.layoutManager = LinearLayoutManager(this)
        listKrp.adapter = krpAdapter

        rasaAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarRasa)
        spRasa.adapter = rasaAdapter
        spRasa.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnFind -> {
                showDataKrp(edNama.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spRasa.setSelection(0)
            pilihRasa = daftarRasa.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihRasa = daftarRasa.get(position)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imgUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataKrp("")
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("id_keripik",edIdkeripik.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_rasa",pilihRasa)
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_keripik",edIdkeripik.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_rasa",pilihRasa)
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_keripik",edIdkeripik.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaRasa(namaRasa : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarRasa.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarRasa.add(jsonObject.getString("nama_rasa"))
                }
                rasaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_rasa",namaRasa)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataKrp(namaKrp : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKrp.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var krp = HashMap<String,String>()
                    krp.put("id_keripik",jsonObject.getString("id_keripik"))
                    krp.put("nama",jsonObject.getString("nama"))
                    krp.put("nama_rasa",jsonObject.getString("nama_rasa"))
                    krp.put("harga",jsonObject.getString("harga"))
                    krp.put("url",jsonObject.getString("url"))
                    daftarKrp.add(krp)
                }
                krpAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama",namaKrp)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataKrp("")
        getNamaRasa("")
    }
}