package dwi.bagas.data_keripik

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_rasa.*
import kotlinx.android.synthetic.main.activity_rasa.btnDelete
import kotlinx.android.synthetic.main.activity_rasa.btnFind
import kotlinx.android.synthetic.main.activity_rasa.btnInsert
import kotlinx.android.synthetic.main.activity_rasa.btnUpdate
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class RasaActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var rasaAdapter : AdapterRasa
    var daftarRasa = mutableListOf<HashMap<String,String>>()
    var idRasa = ""
    val mainUrl = "http://192.168.43.94/data_keripik/"
    val url = mainUrl+"get_nama_rasa.php"
    val url2 = mainUrl+"query_crud_rasa.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                queryRasa("insert")
            }
            R.id.btnUpdate ->{
                queryRasa("update")
            }
            R.id.btnDelete ->{
                queryRasa("delete")
            }
            R.id.btnFind -> {
                showDataRasa(edNamaRasa.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rasa)
        rasaAdapter = AdapterRasa(daftarRasa,this)
        listRasa.layoutManager = LinearLayoutManager(this)
        listRasa.adapter = rasaAdapter
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    fun showDataRasa(namaRasa : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarRasa.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var rasa = HashMap<String,String>()
                    rasa.put("id_rasa",jsonObject.getString("id_rasa"))
                    rasa.put("nama_rasa",jsonObject.getString("nama_rasa"))
                    daftarRasa.add(rasa)
                }
                rasaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_rasa",namaRasa)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryRasa(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataRasa("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id rasa",idRasa)
                        hm.put("nama_rasa",edNamaRasa.text.toString())
                    }
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nama_rasa",edNamaRasa.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_rasa",idRasa)
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataRasa("")
    }

    fun clearInput(){
        idRasa = ""
        edNamaRasa.setText("")
    }
}