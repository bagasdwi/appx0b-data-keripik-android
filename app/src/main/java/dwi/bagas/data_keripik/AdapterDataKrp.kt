package dwi.bagas.data_keripik

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.actvity_keripik.*

class AdapterDataKrp(val dataKrp : List<HashMap<String,String>>, val keripikActivity: KeripikActivity) :
    RecyclerView.Adapter<AdapterDataKrp.HolderDataMhs>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterDataKrp.HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_keripik,p0,false)
        return  HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataKrp.size
    }

    override fun onBindViewHolder(p0: AdapterDataKrp.HolderDataMhs, p1: Int) {
        val data = dataKrp.get(p1)
        p0.txIdkeripik.setText(data.get("id_keripik"))
        p0.txNama.setText(data.get("nama"))
        p0.txRasa.setText(data.get("nama_rasa"))
        p0.txHarga.setText(data.get("harga"))

        p0.cLayout.setOnClickListener({
            val pos = keripikActivity.daftarRasa.indexOf(data.get("nama_rasa"))
            keripikActivity.spRasa.setSelection(pos)
            keripikActivity.edIdkeripik.setText(data.get("id_keripik"))
            keripikActivity.edNama.setText(data.get("nama"))
            keripikActivity.edHarga.setText(data.get("harga"))
            Picasso.get().load(data.get("url")).into(keripikActivity.imgUpload)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }

    class HolderDataMhs(v: View) : RecyclerView.ViewHolder(v){
        val txIdkeripik = v.findViewById<TextView>(R.id.txIdkeripik)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txRasa = v.findViewById<TextView>(R.id.txRasa)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}